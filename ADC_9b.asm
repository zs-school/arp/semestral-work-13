    list	p=16F1508
    #include    "p16f1508.inc"

   
    __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_OFF & _MCLRE_ON & _CP_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF

    __CONFIG _CONFIG2, _WRT_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_ON

;VARIABLE DEFINITIONS
;COMMON RAM 0x70 to 0x7F
    CBLOCK	0x70
	ph
	pl
	carry

	c1
	c2
	c3
	
	temp
	
	num7S			;cislo pro zobrazeni, dalsi 3B budou displeje!
	dispL			;levy 7seg
	dispM			;prostredni 7seg
	dispR			;pravy 7seg
	
	readyL
	readyM
	readyR
	wait
	state
    ENDC
    
;**********************************************************************
	ORG     0x00
  	goto    Start
	
	ORG     0x04
	movlb	.0
	decfsz	state
	goto	Show
			;zero

Clear
	movlw	2
	movwf	state
	movlw	0
	call    SendByte7S
	call    SendByte7S
	call    SendByte7S
	movlw	.1
	
	goto	Out
	
Show	
	movf	readyR,W
	call	SendByte7S
	movf	readyM,W
	call	SendByte7S
	movf	readyL,W
	call	SendByte7S
	movf	wait,W
	
Out	
	movwf	PR2
	bcf	PIR1,TMR2IF	;vynuluje priznak
	movlb	.1
  	retfie
	
Start	movlb	.1		;Banka1
	movlw	b'01101000'	;4MHz Medium
	movwf	OSCCON		;nastaveni hodin

	call	Config_IOs
	call	Config_SPI
	
	movlw	.2
	movwf	state
	movlw	0xFF
	movwf	wait
	
	
	;config ADC
	movlb	.1		;Banka1 s ADC
	movlw	b'00011000'	;P1 = AN6
	movwf	ADCON0
	movlw	b'01110000'	;leftAlig, FRC, VDD
	movwf	ADCON1
	clrf	ADCON2		;single conv.
	bsf	ADCON0,ADON	;zapnout ADC
	
	;config TMR2
	movlb	.0		;Banka1 s TMR2
	movlw	b'00101010'	;1:16 pre, 1:8 post
	movwf	T2CON
	clrf	TMR2		;vynulovat citac
	movlb	.1
	bsf	PIE1,TMR2IE
	bsf	INTCON,GIE	;povolit preruseni jako takove
	bsf	INTCON,PEIE
	movlb	.0
	movlw	0xFF
	movwf	PR2		;nastavit na max. hodnotu
	bsf	T2CON,TMR2ON	;po nastaveni vseho zapnout TMR2
	
	movlb	.1		;Banka1 s ADC

P1	movlw	b'00011000'	;P1 = AN6
	movwf	ADCON0
	bsf	ADCON0,ADON	;zapnout ADC
	bsf     ADCON0,GO       ;start A/D prevodu
        btfsc   ADCON0,GO 	;A/D prevod skoncen?
        goto    $-1             ;pokud ne, navrat o radek vyse

	
Read	movf    ADRESH,W	;nacte nejvyssich 8 bit? vysledku
	movwf	ph
	movf	ADRESL,W
	movwf	pl
	
P2	movlw	b'00101000'	;P2
	movwf	ADCON0
	bsf	ADCON0,ADON	;zapnout ADC
	bsf     ADCON0,GO       ;start A/D prevodu
        btfsc   ADCON0,GO 	;A/D prevod skoncen?
        goto    $-1             ;pokud ne, navrat o radek vyse
	movf	ADRESH,W
	movwf	wait
	bcf	wait,0
	bsf	wait,1
	
Sort	rlf	pl,1
	rlf	pl,0
	andlw	b'00000001'
	movwf	carry
	
	rlf	ph,0
	iorwf	carry,W
	movwf	num7S
	
	movf	ph,W
	andlw	b'10000000'
	movwf	carry
	
        call    Bin2Bcd		;z num7S udela BCD cisla v dispL-dispM-dispR
	
	;p?i?�st carry
	btfsc	carry,7
	call	AddCarry
	
	movf	dispL,W
        call    Byte2Seg	;4bit. cislo ve W zmeni na segment pro zobrazeni
	movwf	dispL
	
	movf	dispM,W
        call    Byte2Seg	;4bit. cislo ve W zmeni na segment pro zobrazeni
	movwf	dispM
	
	movf	dispR,W
        call    Byte2Seg	;4bit. cislo ve W zmeni na segment pro zobrazeni

	
	movwf	readyR
	movf	dispM,W
	movwf	readyM
	movf	dispL,W
	movwf	readyL
	
	goto	P1
	
AddCarry
	movlw	.6
	movwf	c1
	movlw	.5
	movwf	c2
	movlw	.2
	movwf	c3
	
	movf	dispR,W		;dispR do W
	addwf	c1,1		;pricte c1 k W a ulozi do c1
	movlw	.9		;9 do W
	movwf	temp
	movf	c1,W
	subwf	temp,1
	btfsc	STATUS,1
	goto	$+5
	comf	temp,0
	incf	c2,1
	movlw	.10
	subwf	c1,1
	movf	c1,W
	movwf	dispR
	
	movf	dispM,W
	addwf	c2,1
	movlw	.9
	movwf	temp
	movf	c2,W
	subwf	temp,1
	btfsc	STATUS,1
	goto	$+5
	comf	temp,0
	incf	c3,1
	movlw	.10
	subwf	c2,1
	movf	c2,W
	movwf	dispM
	
	movf	c3,W
	addwf	dispL
	return

	
	
    #include	"Config_IOs.inc"
    
    #include	"Display.inc"
		
	END

		
		
		


